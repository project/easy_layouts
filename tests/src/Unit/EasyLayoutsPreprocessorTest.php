<?php

namespace Drupal\Tests\easy_layouts\Unit;

use Drupal\Core\Template\Attribute;
use Drupal\Tests\UnitTestCase;
use Drupal\easy_layouts\EasyLayoutsPreprocessor;

/**
 * Test Easy Layout Preprocessor.
 */
class EasyLayoutsPreprocessorTest extends UnitTestCase {

  /**
   * Test that user is not editing a layout.
   */
  public function testUserIsNotEditing() {
    $ep = new EasyLayoutsPreprocessor($this->getVariables(FALSE));
    $this->assertEquals(FALSE, $ep->getIsEditing());
  }

  /**
   * Test that user is editing the layout.
   */
  public function testUserIsEditing() {
    $ep = new EasyLayoutsPreprocessor($this->getVariables());
    $this->assertEquals(TRUE, $ep->getIsEditing());
  }

  /**
   * Test parse attributes.
   */
  public function testParseAttributes() {
    $ep = new EasyLayoutsPreprocessor([]);
    $string = 'class|test-class,id|test-id,role|form';
    $attributes = $ep->parseAttributes($string);
    $this->assertEquals($attributes['class'], 'test-class');
    $this->assertEquals($attributes['id'], 'test-id');
    $this->assertEquals($attributes['role'], 'form');
  }

  /**
   * Test process variables for layout.
   */
  public function testProcessVariablesForLayout() {
    $ep = new EasyLayoutsPreprocessor($this->getVariables(FALSE));
    $variables = $ep->processVariablesForLayout();

    $this->assertEquals(FALSE, $variables['is_editing']);

    foreach (EasyLayoutsPreprocessor::EASY_LAYOUTS_ELEMENTS as $element) {
      $attributes = $variables['settings'][$element]['attributes']->toArray();
      $this->assertEquals($attributes['class'], [
        0 => 'new-class',
        1 => 'main-class',
      ]);

      $this->assertEquals($attributes['id'], 'new-id');
      $this->assertEquals($attributes['role'], 'form');
    }

    $ep = new EasyLayoutsPreprocessor($this->getVariables(TRUE));
    $variables = $ep->processVariablesForLayout();
    $this->assertEquals(TRUE, $variables['is_editing']);

    // Verify row got extra classes.
    $attributes = $variables['settings']['row']['attributes']->toArray();
    $this->assertEquals($attributes['class'], [
      0 => 'new-class',
      1 => 'main-class',
      2 => 'row',
      3 => 'two-column-layout',
    ]);
  }

  /**
   * Tests attribute classes added on root are added to the outer wrapper.
   */
  public function testAddingRootAttributesToOuterWrappers() {
    $variables = $this->getVariables(FALSE);
    $variables['attributes'] = [
      'class' => [
        'custom-item-1',
        'custom-item-2',
      ],
      'role' => [
        'new-role',
      ],
    ];

    $variables['settings']['wrapper']['enabled'] = TRUE;

    $classes_2 = [
      0 => 'new-class',
      1 => 'main-class',
    ];

    $classes_4 = [
      0 => 'new-class',
      1 => 'main-class',
      2 => 'custom-item-1',
      3 => 'custom-item-2',
    ];

    // Verify only new classes were added to the wrapper.
    $ep = new EasyLayoutsPreprocessor($variables);
    $new_variables = $ep->processVariablesForLayout();
    $this->verifyHasClasses($new_variables, 'wrapper', $classes_4);
    $this->verifyHasClasses($new_variables, 'container', $classes_2);
    $this->verifyHasClasses($new_variables, 'row', $classes_2);
    $this->verifyHasClasses($new_variables, 'column', $classes_2);

    // Verify only new classes were added to the container.
    $variables['settings']['wrapper']['enabled'] = FALSE;
    $variables['settings']['container']['enabled'] = TRUE;
    $ep = new EasyLayoutsPreprocessor($variables);
    $new_variables = $ep->processVariablesForLayout();
    $this->verifyHasClasses($new_variables, 'wrapper', $classes_2);
    $this->verifyHasClasses($new_variables, 'container', $classes_4);
    $this->verifyHasClasses($new_variables, 'row', $classes_2);
    $this->verifyHasClasses($new_variables, 'column', $classes_2);

    // Verify only new classes were added to the row.
    $variables['settings']['wrapper']['enabled'] = FALSE;
    $variables['settings']['container']['enabled'] = FALSE;
    $variables['settings']['row']['enabled'] = TRUE;
    $ep = new EasyLayoutsPreprocessor($variables);
    $new_variables = $ep->processVariablesForLayout();
    $this->verifyHasClasses($new_variables, 'wrapper', $classes_2);
    $this->verifyHasClasses($new_variables, 'container', $classes_2);
    $this->verifyHasClasses($new_variables, 'row', $classes_4);
    $this->verifyHasClasses($new_variables, 'column', $classes_2);

    // Verify only new classes were added to the column.
    $variables['settings']['wrapper']['enabled'] = FALSE;
    $variables['settings']['container']['enabled'] = FALSE;
    $variables['settings']['row']['enabled'] = FALSE;
    $variables['settings']['column']['enabled'] = TRUE;
    $ep = new EasyLayoutsPreprocessor($variables);
    $new_variables = $ep->processVariablesForLayout();
    $this->verifyHasClasses($new_variables, 'wrapper', $classes_2);
    $this->verifyHasClasses($new_variables, 'container', $classes_2);
    $this->verifyHasClasses($new_variables, 'row', $classes_2);
    $this->verifyHasClasses($new_variables, 'column', $classes_4);
  }

  /**
   * Verifies classes.
   *
   * @param array $variables
   *   Variables array.
   * @param string $key
   *   Key of settings you want to test.
   * @param array $classes
   *   Array of classes of intended results.
   */
  protected function verifyHasClasses(array $variables, $key, array $classes) {
    $attributes = $variables['settings'][$key]['attributes']->toArray();
    $this->assertEquals($attributes['class'], $classes);
  }

  /**
   * Create variables for testing.
   *
   * @param bool $is_editing
   *   Set to TRUE to add data region data.
   *
   * @return array
   *   Variables array.
   */
  protected function getVariables($is_editing = TRUE) {
    $variables = [];
    $variables['region_attributes'] = [];
    if ($is_editing) {
      $variables['region_attributes'][] = new Attribute(['data-region' => ['test']]);
    }
    $variables['theme_hook_original'] = 'two_column_layout';
    foreach (EasyLayoutsPreprocessor::EASY_LAYOUTS_ELEMENTS as $element) {
      $variables['settings'][$element]['enabled'] = TRUE;
      $variables['settings'][$element]['element'] = 'div';
      $variables['settings'][$element]['class'] = 'main-class';
      $variables['settings'][$element]['attributes'] = 'class|new-class,id|new-id,role|form';
    }

    return $variables;
  }

}
