<?php

namespace Drupal\easy_layouts;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides form elements for Easy Layouts.
 */
class EasyLayoutsFormElements {

  use StringTranslationTrait;

  /**
   * Element options.
   *
   * @var array
   */
  const EASY_LAYOUTS_ELEMENT_OPTIONS = [
    'div' => 'Div',
    'span' => 'Span',
    'section' => 'Section',
    'article' => 'Article',
    'header' => 'Header',
    'footer' => 'Footer',
    'aside' => 'Aside',
    'figure' => 'Figure',
  ];

  /**
   * Default values.
   *
   * @var array
   */
  const EASY_LAYOUTS_DEFAULTS = [
    'wrapper' => [
      'enabled' => FALSE,
      'element' => 'div',
      'class' => 'wrapper',
      'attributes' => '',
    ],
    'container' => [
      'enabled' => TRUE,
      'element' => 'div',
      'class' => 'container',
      'attributes' => '',
    ],
    'row' => [
      'enabled' => TRUE,
      'element' => 'div',
      'class' => 'row',
      'attributes' => '',
    ],
    'column' => [
      'enabled' => TRUE,
      'element' => 'div',
      'class' => 'col',
      'attributes' => '',
    ],
  ];

  /**
   * Adds the easy layout elements to the form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param array $configuration
   *   Configuration array.
   *
   * @return array
   *   Updated form element.
   */
  public function getEasyLayoutFormElements(array $form, array $configuration) {
    foreach (EasyLayoutsPreprocessor::EASY_LAYOUTS_ELEMENTS as $element) {
      $form[$element] = [
        '#type' => 'details',
        '#open' => TRUE,
        '#title' => $this->t(':wrapper', [
          ':wrapper' => strtoupper($element),
        ]),
        '#tree' => TRUE,
      ];

      $form[$element]['enabled'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Add :wrapper element', [
          ':wrapper' => $element,
        ]),
        '#default_value' => $configuration[$element]['enabled'],
      ];

      $form[$element]['element'] = [
        '#type' => 'select',
        '#title' => $this->t('Element'),
        '#options' => self::EASY_LAYOUTS_ELEMENT_OPTIONS,
        '#default_value' => $configuration[$element]['element'],
        '#states' => [
          'visible' => [
            ':input[name="layout_settings[' . $element . '][enabled]"]' => [
              ['checked' => TRUE],
            ],
          ],
        ],
      ];

      $form[$element]['class'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Class'),
        '#default_value' => $configuration[$element]['class'],
        '#states' => [
          'visible' => [
            ':input[name="layout_settings[' . $element . '][enabled]"]' => [
              ['checked' => TRUE],
            ],
          ],
        ],
      ];

      $form[$element]['attributes'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Additional attributes'),
        '#description' => $this->t('E.g. id|custom-id,role|form,data-item|some value'),
        '#default_value' => $configuration[$element]['attributes'],
        '#states' => [
          'visible' => [
            ':input[name="layout_settings[' . $element . '][enabled]"]' => [
              ['checked' => TRUE],
            ],
          ],
        ],
      ];
    }

    return $form;
  }

  /**
   * Get default configurations.
   *
   * @return array
   *   Array of default configurations.
   */
  public function getDefaultConfigurations() {
    $config = \Drupal::config('easy_layouts.settings');

    // Get default values in an array.
    $defaultConfiguration = EasyLayoutsFormElements::EASY_LAYOUTS_DEFAULTS;
    foreach (EasyLayoutsPreprocessor::EASY_LAYOUTS_ELEMENTS as $element) {
      if ($config->get($element)) {
        $defaultConfiguration[$element] = $config->get($element);
      }
    }

    return $defaultConfiguration;
  }

}
