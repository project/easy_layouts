<?php

namespace Drupal\easy_layouts\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\easy_layouts\EasyLayoutsFormElements;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigManager;

/**
 * Module settings form.
 */
class ModuleSettingsForm extends FormBase {

  /**
   * Drupal\Core\Config\ConfigManager definition.
   *
   * @var \Drupal\Core\Config\ConfigManager
   */
  protected $configManager;

  /**
   * Constructs a new RestrictionPluginConfigForm object.
   */
  public function __construct(ConfigManager $config_manager) {
    $this->configManager = $config_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'easy_layouts_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['markup'] = [
      '#markup' => '<h2>' . $this->t('These settings are only for defaults. Previously created items will not be affected.') . '</h2>',
    ];
    $easyLayoutsFormElements = new EasyLayoutsFormElements();
    $form = $easyLayoutsFormElements->getEasyLayoutFormElements($form, $easyLayoutsFormElements->getDefaultConfigurations());
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save configuration'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory()->getEditable('easy_layouts.settings');
    $config->set('wrapper', $form_state->getValue('wrapper'));
    $config->set('container', $form_state->getValue('container'));
    $config->set('row', $form_state->getValue('row'));
    $config->set('column', $form_state->getValue('column'));
    $config->save();
  }

}
