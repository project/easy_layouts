<?php

namespace Drupal\easy_layouts\Plugin\Layout;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Layout\LayoutDefault;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Component\Utility\Xss;
use Drupal\easy_layouts\EasyLayoutsFormElements;

/**
 * Easy Layouts configurations.
 */
class EasyLayoutsBase extends LayoutDefault implements PluginFormInterface {

  /**
   * Form elements object.
   *
   * @var \Drupal\easy_layouts\EasyLayoutsFormElements
   */
  protected $easyLayoutsFormElements;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    $this->easyLayoutsFormElements = new EasyLayoutsFormElements();
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + $this->easyLayoutsFormElements->getDefaultConfigurations();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $configuration = $this->getConfiguration();
    $form = $this->easyLayoutsFormElements->getEasyLayoutFormElements($form, $configuration);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['wrapper'] = $form_state->getValue('wrapper');
    $this->configuration['container'] = $form_state->getValue('container');
    $this->configuration['row'] = $form_state->getValue('row');
    $this->configuration['column'] = $form_state->getValue('column');

    // Filter the html attributes.
    $this->configuration['wrapper']['attributes'] = Xss::filter($this->configuration['wrapper']['attributes']);
    $this->configuration['container']['attributes'] = Xss::filter($this->configuration['container']['attributes']);
    $this->configuration['row']['attributes'] = Xss::filter($this->configuration['row']['attributes']);
    $this->configuration['column']['attributes'] = Xss::filter($this->configuration['col']['attributes']);
  }

}
