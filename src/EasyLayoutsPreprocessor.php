<?php

namespace Drupal\easy_layouts;

use Drupal\Core\Template\Attribute;
use Drupal\Component\Utility\Html;

/**
 * Handles pre-processing of Easy Layout's variables.
 */
class EasyLayoutsPreprocessor {

  /**
   * Preprocess variables.
   *
   * @var array
   */
  protected $variables;

  /**
   * Is Editing.
   *
   * @var bool
   */
  protected $isEditing = FALSE;

  /**
   * Set to true if we merged the attributes already.
   *
   * @var bool
   */
  protected $mergedRootLevelAttributes = FALSE;

  /**
   * Elements that contain settings.
   *
   * @var array
   */
  const EASY_LAYOUTS_ELEMENTS = ['wrapper', 'container', 'row', 'column'];

  /**
   * EasyLayoutsVariables constructor.
   *
   * @param array $variables
   *   Preprocess variables for layout.
   */
  public function __construct(array $variables) {
    $this->variables = $variables;
    $this->setIsEditing();
  }

  /**
   * When a user is editing the layout set to true.
   */
  protected function setIsEditing() {
    $isViewingInLayoutBuilder = FALSE;
    if (isset($this->variables['region_attributes'])) {
      foreach ($this->variables['region_attributes'] as $region) {
        // Found that if data-region is set to true that means the user is
        // editing the layout.
        if ($region->hasAttribute('data-region')) {
          $isViewingInLayoutBuilder = TRUE;
        }
      }
    }

    $this->isEditing = $isViewingInLayoutBuilder;
  }

  /**
   * Is user editing the layout.
   *
   * @return bool
   *   Set to true if user is editing layout.
   */
  public function getIsEditing() {
    return $this->isEditing;
  }

  /**
   * Parses an attribute string saved in the UI.
   *
   * @param string $string
   *   The attribute string to parse.
   *
   * @return array
   *   A parsed attributes array.
   */
  public function parseAttributes($string) {
    $attributes = [];
    if ($string) {
      $parts = explode(',', $string);
      foreach ($parts as $attribute) {
        if (strpos($attribute, '|') !== FALSE) {
          list($key, $value) = explode('|', $attribute);
          $attributes[$key] = $value;
        }
      }
    }

    return $attributes;
  }

  /**
   * Process the variables for layout.
   *
   * @return array
   *   Updated variables.
   */
  public function processVariablesForLayout() {
    $this->variables['is_editing'] = $this->getIsEditing();

    foreach (self::EASY_LAYOUTS_ELEMENTS as $element) {
      $this->updateAttributes($element);
    }

    if ($this->isEditing) {
      $this->updateItemsForEditing();
    }

    return $this->variables;
  }

  /**
   * Update attributes to convert array to Attribute objects.
   */
  protected function updateAttributes($key) {
    if (!isset($this->variables['settings'][$key])) {
      return;
    }

    $settings = $this->variables['settings'][$key] ?? [];
    $attributes = new Attribute($this->parseAttributes($settings['attributes']));

    if ($settings['class']) {
      $attributes->addClass($settings['class']);
    }

    // If classes are added on the root attribute level, add them in.
    // @TODO use merge when it is ready
    // https://www.drupal.org/project/drupal/issues/2623960.
    if ($rootAttributes = $this->variables['attributes']['class'] ?? []) {
      if (!$this->mergedRootLevelAttributes && $this->variables['settings'][$key]['enabled'] == TRUE) {
        $attributes->addClass($rootAttributes);
        $this->mergedRootLevelAttributes = TRUE;
      }
    }

    $this->variables['settings'][$key]['attributes'] = $attributes;
  }

  /**
   * Make any changes for the layout during editing.
   */
  protected function updateItemsForEditing() {
    // Enable the row and column classes are enabled, just in case they were
    // disabled in the UI so we can have drag and drop work properly in UI.
    $this->variables['settings']['row']['enabled'] = TRUE;
    $this->variables['settings']['column']['enabled'] = TRUE;

    // Add region classes so they can be styles in the UI when editing.
    $this->variables['settings']['row']['attributes']->addClass('row');
    if (isset($this->variables['theme_hook_original'])) {
      $this->variables['settings']['row']['attributes']->addClass(Html::cleanCssIdentifier($this->variables['theme_hook_original']));
    }
    $this->variables['settings']['column']['attributes']->addClass('col');
  }

}
