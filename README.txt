CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

The Easy Layouts module provides a super easy and flexible way to control your
layouts with Layout Builder. It provides you the ability to add wrappers,
containers, rows or columns to sections in your layouts.

REQUIREMENTS
------------

This module requires the following module:

 * Layout Builder (Core)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

The module will work without any configuration but you have the ability to set
the default settings for any new sections added in Administration »
Configuration » Easy Layouts Settings

MAINTAINERS
-----------

Current maintainers:
 * Albert Jankowski (albertski) - https://www.drupal.org/u/albertski
